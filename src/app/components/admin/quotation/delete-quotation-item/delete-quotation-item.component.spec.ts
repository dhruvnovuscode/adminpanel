import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteQuotationItemComponent } from './delete-quotation-item.component';

describe('DeleteQuotationItemComponent', () => {
  let component: DeleteQuotationItemComponent;
  let fixture: ComponentFixture<DeleteQuotationItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteQuotationItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteQuotationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
