import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusremarksComponent } from './statusremarks.component';

describe('StatusremarksComponent', () => {
  let component: StatusremarksComponent;
  let fixture: ComponentFixture<StatusremarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusremarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusremarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
