import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LeadsService } from 'src/app/shared/services/adminpanel/leads/leads.service';
import { Leads } from 'src/app/shared/models/adminpanel/leads/leads';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';
import { DeleteLeadComponent } from './delete-lead/delete-lead.component';
import { Employee } from 'src/app/shared/models/adminpanel/employee';
import { EmployeeListService } from 'src/app/shared/services/adminpanel/employee-list.service';
import { MatOptionSelectionChange } from '@angular/material/core';
import { CONSTANT } from 'src/app/shared/utils/constant';
import { StatusremarksComponent } from './statusremarks/statusremarks.component';

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.scss']
})

export class LeadsComponent implements OnInit {
  public variables = ['Parth Shah', 'Tushar Panchal', 'Yash Shah', 'Jay Patel', 'Dhruv Boghani', 'pre'];
  // public filteredList1 = this.variables.slice();
  filteredList1
  isLoading;

  public variables2 = CONSTANT.status;
  public filteredList2 = this.variables2.slice();
  toppings = new FormControl(this.variables2);

  displayedColumns: string[] = ['checkbox', 'position', 'name', 'actions', 'company', 'email', 'phone', 'whatsappno', 'leadvalue', 'tags', 'source', 'assigned', 'status'];
  dataSource = new MatTableDataSource<Leads>([]);
  selection = new SelectionModel<Leads>(true, []);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  color: any;
  Leads: any;
  employee: any;
  filterSelectObj = [];
  filteredListEmployee: any;

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private LeadServices: LeadsService,
    private EmployeeListService: EmployeeListService,
    private matSnack: SnackbarService
  ) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    // console.log(this.filteredList1)

    this.getLeads();
    this.getEmployee();
    localStorage.removeItem('edit-lead');
    localStorage.removeItem('view-lead');
  }

  ngAfterViewInit() {
    //  Scrolls to top of Page after page view initialized
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

  handleMouseOver(row) {
    const position = row;
    this.dataSource.data.map((data: any) => {
      if (data === position) {
        data.show = true;
      }
    });
  }

  handleMouseLeave(row) {
    const position = row;
    this.dataSource.data.map((data: any) => {
      if (data === position) {
        data.show = false;
      }
    });
  }


  viewLead(event) {
    localStorage.setItem('leadData', JSON.stringify(event))

    localStorage.setItem('view-lead', 'View-Lead')
    this.router.navigateByUrl('admin/leads/view-lead', { state: event });

  }

  editLead(event) {
    localStorage.setItem('leadData', JSON.stringify(event))
    localStorage.setItem('edit-lead', 'Edit-Lead')
    this.router.navigateByUrl('admin/leads/view-lead', { state: event });

  }

  //Add lead page route
  addLead() {
    this.router.navigateByUrl('admin/leads/add-lead');
  }

  //Import lead page route
  importLead() {
    this.router.navigateByUrl('admin/leads/import-lead');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.Leads.forEach(row =>
        this.selection.select(row)
      );
  }

  logSelection() {
    this.selection.selected.forEach(s => console.log(s.name));
  }

  someMethod(event) {
    console.log(event)
    const filterValue = event;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  someMethod1(event) {
  
    console.log(event)

    const filterValue = event;
    this.dataSource.filter = filterValue.trim().toLowerCase();
   
  }
  openremarksDialog(value,id) {
    
     const dialogRef = this.dialog.open(StatusremarksComponent, {
      height: 'auto',
      width: '30%',
      data: { id: id, value: value }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getLeads()
      // this.ChangeStatus(event, status)
    });
  }

  /*
    Get Leads list api called
  */
  getLeads() {
    this.isLoading = true;
    this.LeadServices.getLeads()
      .subscribe((data: Leads[]) => {
        this.isLoading = false;
        this.Leads = data;
        this.dataSource.data = data
      });
  }

  //Change status of lead
  ChangeStatus(event, status) {
    console.log(status)
    this.openremarksDialog(event.value, status.id)
    // this.LeadServices.ChangeLeadStatus(`leadstatus/${status.id}`, event.value)
    //   .subscribe((res) => {
    //     console.log(res)
    //     this.matSnack.showNotification("Status Updated", "")
    //   });
  }

  /*
    Delete Leads Popup
  */
  deleteLead(element) {
    const dialogRef = this.dialog.open(DeleteLeadComponent, {
      height: 'auto',
      width: '30%',
      data: element
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getLeads()
      this.getLeads()
    });

  }


  /*
   Get employee list api called
 */
  getEmployee() {
    this.EmployeeListService.getEmployee()
      .subscribe((data: Employee[]) => {
        this.employee = data;
        // this.variables = this.employee.find(x => x.name)
        this.filteredListEmployee = data
        this.filteredList1 = this.filteredListEmployee.slice()
      });
  }



  //Assign Employee of lead
  AssignedEmployee(event, status) {
    this.LeadServices.AssignedEmployee(`leadassigned/${status.id}`, event.source.triggerValue, event.value)
      .subscribe((res) => {
        // console.log(event.value)
        console.log(res)
        this.matSnack.showNotification("Assign Employee Updated", "")
      });
  }
}
