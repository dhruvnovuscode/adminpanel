import { Component, OnInit, ViewChild } from '@angular/core';

import * as XLSX from 'xlsx';
import { NgxCsvParser } from 'ngx-csv-parser';
import { NgxCSVParserError } from 'ngx-csv-parser';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { LeadsService } from 'src/app/shared/services/adminpanel/leads/leads.service';
import { first } from 'rxjs/operators';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

type AOA = any[][];

@Component({
  selector: 'app-import-lead',
  templateUrl: './import-lead.component.html',
  styleUrls: ['./import-lead.component.scss']
})

export class ImportLeadComponent {

  @ViewChild('fileImportInput', { static: false }) fileImportInput: any;
  csvFile: any;
  csvFileData: any;

  myForm = new FormGroup({
    name: new FormControl('',),
    file: new FormControl('',),
    fileSource: new FormControl('',)
  });

  constructor(
    private matSnack: SnackbarService,
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit() {
    //  Scrolls to top of Page after page view initialized
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

  get f() {
    return this.myForm.controls;
  }

  //File change function
  onFileChange(event) {

    if (event.target.files.length > 0) {
      this.csvFileData = event.target.files[0]
      const file = event.target.files[0];
      this.myForm.patchValue({
        fileSource: file
      });
    }
  }

  //Upload excel file api call
  submit() {
    const formData = new FormData();
    formData.append('uploadfile', this.myForm.get('fileSource').value);

    this.http.post('https://crmnovuscode.herokuapp.com/uploadfile', formData)
      .subscribe(res => {
        this.router.navigateByUrl('/admin/leads');
        this.matSnack.showNotification('New Lead Added', '')
      })
  }

  //Export sample file
  export() {
    window.open('/assets/images/sample_import_file.csv', '_blank');
  }
}
