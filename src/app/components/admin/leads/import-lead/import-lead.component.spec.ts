import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportLeadComponent } from './import-lead.component';

describe('ImportLeadComponent', () => {
  let component: ImportLeadComponent;
  let fixture: ComponentFixture<ImportLeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportLeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportLeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
