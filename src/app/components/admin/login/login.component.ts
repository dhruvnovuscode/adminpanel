import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { CONSTANT } from 'src/app/shared/utils/constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  loginFormGroup: FormGroup;
  txtEmail: any;
  txtPassword: any;
  error: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    this.loginFormGroup = this.fb.group({
      password: new FormControl('', Validators.required),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(CONSTANT.emailPattern),
      ]),
    });
    this.txtEmail = this.loginFormGroup.get('email');
    this.txtPassword = this.loginFormGroup.get('password');
    localStorage.removeItem('email-change')
    localStorage.removeItem('email-change-token')

  }

  // Loginform controls
  get alf() { return this.loginFormGroup.controls; }

  //Login api call
  login() {
    if (this.loginFormGroup.invalid) {

      return;
    }

    this.authenticationService.login(this.alf.email.value, this.alf.password.value)
      .pipe(first())
      .subscribe(
        (data: any) => {
          console.log(data,"login data")
          localStorage.setItem('email-change', this.alf.email.value)
          var productright = data.productright
          localStorage.setItem('productright', productright)
          var quotationright = data.quotationright
          localStorage.setItem('quotationright', quotationright)
          if (data.role === 'Employee') {
            this.router.navigate(['employee/leads']);
          } else {
            this.router.navigate(['admin/leads']);
          }
        },
        error => {
          this.error = error.error.message;
        });
  }
}
