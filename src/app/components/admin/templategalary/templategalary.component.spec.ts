import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplategalaryComponent } from './templategalary.component';

describe('TemplategalaryComponent', () => {
  let component: TemplategalaryComponent;
  let fixture: ComponentFixture<TemplategalaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplategalaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplategalaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
