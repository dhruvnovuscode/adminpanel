import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-templategalary',
  templateUrl: './templategalary.component.html',
  styleUrls: ['./templategalary.component.scss']
})
export class TemplategalaryComponent  {
  title = 'Templates';
  titleSelected;
  gridColumns = 4;
  changeText: boolean;
  constructor() {
     this.changeText = false;
  }
  toggleGridColumns() {
    // this.gridColumns = this.gridColumns === 3 ? 4 : 3;
    this.gridColumns ===  4 ;
  }
  select(title) {
    console.log(title)
    this.titleSelected = title;
  }
  page = 0;
  size = 8;
  data = [];
  data2 = [
    {id:1, url: 'https://images.freeimages.com/images/large-previews/996/easter-1399885.jpg'},
    {id:2, url: 'https://images.freeimages.com/images/large-previews/0b3/burning-tree-1377053.jpg'},
    {id:3, url: 'https://images.freeimages.com/images/large-previews/346/cemetery-1404186.jpg'},
    {id:4, url: 'https://images.freeimages.com/images/large-previews/310/resting-peacefully-1574880.jpg'},
    {id:5, url: 'https://images.freeimages.com/images/large-previews/996/easter-1399885.jpg'},
    {id:6, url: 'https://images.freeimages.com/images/large-previews/996/easter-1399885.jpg'},
    {id:7, url: 'https://images.freeimages.com/images/large-previews/0b3/burning-tree-1377053.jpg'},
    {id:8, url: 'https://images.freeimages.com/images/large-previews/346/cemetery-1404186.jpg'},
    {id:9, url: 'https://images.freeimages.com/images/large-previews/346/cemetery-1404186.jpg'},
    {id:2, url: 'https://images.freeimages.com/images/large-previews/310/resting-peacefully-1574880.jpg'},
    {id:3, url: 'https://images.freeimages.com/images/large-previews/346/cemetery-1404186.jpg'},
    {id:4, url: 'https://images.freeimages.com/images/large-previews/310/resting-peacefully-1574880.jpg'},
    {id:5, url: 'https://images.freeimages.com/images/large-previews/0b3/burning-tree-1377053.jpg'},
    {id:6, url: 'https://images.freeimages.com/images/large-previews/996/easter-1399885.jpg'},
    {id:7, url: 'https://images.freeimages.com/images/large-previews/0b3/burning-tree-1377053.jpg'},
    {id:8, url: 'https://images.freeimages.com/images/large-previews/346/cemetery-1404186.jpg'}
  ];
  ngOnInit() {
    this.getData({pageIndex: this.page, pageSize: this.size});
  }

  getData(obj) {
    let index=0,
        startingIndex=obj.pageIndex * obj.pageSize,
        endingIndex=startingIndex + obj.pageSize;

    this.data = this.data2.filter(() => {
      index++;
      return (index > startingIndex && index <= endingIndex) ? true : false;
    });
  }

  
  
}