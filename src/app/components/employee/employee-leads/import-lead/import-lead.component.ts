import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';
import { CONSTANT } from 'src/app/shared/utils/constant';

@Component({
  selector: 'app-import-lead',
  templateUrl: './import-lead.component.html',
  styleUrls: ['./import-lead.component.scss']
})
export class ImportLeadComponent implements OnInit {
  public variables = ['Parth Shah', 'Tushar Panchal', 'Yash Shah', 'Jay Patel', 'Dhruv Boghani'];
  public filteredList1 = this.variables.slice();

  public variables2 = CONSTANT.status;
  public filteredList2 = this.variables2.slice();
  public variables3 = ['Demo', 'Facebook', 'calling'];
  public filteredList3 = this.variables3.slice();
  fileName: string = 'SheetJS.xlsx';
  displayedColumns: string[] = ['name', 'position', 'company', 'description', 'country', 'zip', 'city', 'state', 'address', 'email', 'website', 'phone', 'whatsappno', 'leadvalue', 'tags', 'servicecategory', 'aadharcard',];
  dataSource = new MatTableDataSource<any>([]);
  csvRecords: any[] = [];
  header = true;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('fileImportInput', { static: false }) fileImportInput: any;
  csvFile: any;
  csvFileData: any;

  myForm = new FormGroup({
    name: new FormControl('',),
    file: new FormControl('',),
    fileSource: new FormControl('',)
  });
  userinfo: any;
  userId: string;

  constructor(
    private matSnack: SnackbarService, private http: HttpClient,
    private router: Router) {
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.userinfo = JSON.parse(localStorage.getItem('currentUser'));
    this.userId = this.userinfo.userid
    console.log(this.userinfo.userid)
  }

  ngAfterViewInit() {
    //  Scrolls to top of Page after page view initialized
    let top = document.getElementById('top');
    if (top !== null) {
      top.scrollIntoView();
      top = null;
    }
  }

  get f() {
    return this.myForm.controls;
  }

  //File change function
  onFileChange(event) {

    if (event.target.files.length > 0) {
      this.csvFileData = event.target.files[0]
      const file = event.target.files[0];
      this.myForm.patchValue({
        fileSource: file
      });
    }
  }

  //Upload file api call
  submit() {
    // console.log(encodeURIComponent(this.userId))
    const formData = new FormData();
    formData.append('employeeleadfile', this.myForm.get('fileSource').value);

    this.http.post(`https://crmnovuscode.herokuapp.com/employeeleadfile?userid='${this.userId}'`, formData)
      .subscribe(res => {
        this.router.navigateByUrl('/employee/leads');
        this.matSnack.showNotification('New Lead Added', '')
      })
  }

  //Export sample file
  export() {
    window.open('/assets/images/sample_import_file.csv', '_blank');
  }

}
