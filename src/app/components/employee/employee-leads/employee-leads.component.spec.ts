import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeLeadsComponent } from './employee-leads.component';

describe('EmployeeLeadsComponent', () => {
  let component: EmployeeLeadsComponent;
  let fixture: ComponentFixture<EmployeeLeadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeLeadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeLeadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
