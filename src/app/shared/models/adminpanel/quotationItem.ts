export class QuotationItem {
    Items:string;
    Description:string;
    Qty:string;
    Rate:number;
    Tax:string;
    quotationId:string;
}