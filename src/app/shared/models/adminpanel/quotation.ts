export class Quotation {
    email: string;
    phone: string;
    subject: string;
    related: string;
    date:string;
    opentill:string;
    discounttype:any;
    favoritetags:any;
    status:any;
    assigned: any;
    toperson:string;
    address: string;
    city: string;
    state: string;
    country: string;
    zip: string;
    total: string;
    quotationitem:{
        item:string;
        description:string;
        qty:string;
        rate:string;
        tax:string;
    };
    discount:string;
    adjustment:string;
}
