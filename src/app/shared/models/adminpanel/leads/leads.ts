export class Leads {
    id: number;
    name: string;
    CompanyName: string;
    email: string;
    phone: string;
    whatsappnumber: string;
    leadvalue: string;
    tags: string;
    assigned: string;
    Status: string;
    address: string;
    city: string;
    state: string;
    country: string;
    zip: string;
    language: string;
    description: string;
    date_contact: string;
    created_at: string;
    updated_at: string
}
