export class Reminders {
    created_at: string;
    description: string;
    id: number;
    leadId: string;
    reminder_date: string;
    reminder_to: string;
    send_mail: string;
    updated_at: string;
}
