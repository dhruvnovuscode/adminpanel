export class Notes {
    created_at: string;
    date_contacted: string;
    id: number;
    leadId: string;
    note: string;
    updated_at: string;
}
