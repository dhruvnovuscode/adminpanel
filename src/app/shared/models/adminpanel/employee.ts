export class Employee {

    email: string;
    name: string;
    phone: string;
    profile_image: string;
    facebook: string;
    linkedin: string;
    skype: string;
    language: string;
    password: string;
    role: string;
    uotationright:boolean;
    productright:boolean;
}
