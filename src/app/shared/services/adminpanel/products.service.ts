import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Products } from '../../models/adminpanel/products';
import { RestmanagerService } from '../../utils/restmanagerService';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private restmanagerService: RestmanagerService;
  _apiEndPoint: string = '';

  constructor(private http: HttpClient,
    restmanagerService: RestmanagerService) {
    this.restmanagerService = restmanagerService;
    this._apiEndPoint = environment.Url;
  }

  //Add Products api
  AddProduct(item: string, rate: string, tax: string,assigned:string,assigneuserid:string) {
    return this.http.post<Products>(`${environment.apiUrl}/product`, {
      item, rate, tax,assigned,assigneuserid
    })
      .pipe(map(res => {
        return res;
      }));
  }

  //Update Products api
  UpdateProduct(apiURL: string, id: string, item: string, rate: string, tax: string) {
    return this.http.put<Products>(`${environment.apiUrl}/${apiURL}`, {
      id, item, rate, tax
    })
      .pipe(map(res => {
        return res;
      }));
  }


  //Get Products-list api
  getProducts() {
    return this.http.get<Products[]>(`${environment.apiUrl}/product`);
  }

  //Get Product data by id api
  getProductdatabyid<T>(apiURL: string): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.get<T[]>(`${this._apiEndPoint}/${apiURL}`)
    );
  }

    //Prdouct Assign  status  api
    AssignedProduct<T>(apiURL: string, assigned: any, assigneuserid: any): Observable<T[]> {

      return this.mapAndCatchError(
        this.http.put<T[]>(`${this._apiEndPoint}/${apiURL}`, {
          assigned,
          assigneuserid
        })
      );
    }
  private mapAndCatchError<TData>(response: Observable<TData>): Observable<TData> {

    return response.pipe(
      map((r: TData) => {

        return r;
      }),
      catchError((error: HttpErrorResponse) => {
        return of(null);
      })
    );
  }

  //Delete Product  api
  DeleteProduct<T>(apiURL: string): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.delete<T[]>(`${this._apiEndPoint}/${apiURL}`, {
      })
    );
  }
}
