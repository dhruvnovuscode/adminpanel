import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Quotation } from '../../models/adminpanel/quotation';
import { QuotationItem } from '../../models/adminpanel/quotation_item';
@Injectable({
  providedIn: 'root'
})
export class AddQuotationService {
  _apiEndPoint: string = '';
  constructor(private http: HttpClient,) { 
    this._apiEndPoint = environment.Url;
  }
    //Get quotation-list api
    getQuotation() {
      return this.http.get<any[]>(`${environment.apiUrl}/quotation`);
    }
      //Get quotation data by id api
    getQuotationById<T>(apiURL: string): Observable<T[]> {
    return this.mapAndCatchError(
      this.http.get<T[]>(`${this._apiEndPoint}/${apiURL}`)
    );
  }
    //Add quotation api
    AddQuotation(Email: string,
      Phone: string,
      Subject: string,
      Related:string,
      date:string, 
      OpenTill:string,
      DiscountType:string,
      Favoritetags:string,
      Status:string,
      Assigned: string,
      Toperson:string,
      Address: string,
      City: string,
      State: string,
      Country: string,
      ZipCode: string,
      Total: string,Quatation_items,Discount:string,Adjustment:string) {
      return this.http.post<Quotation>(`${environment.apiUrl}/quotationCreate`, {
        Email,Phone,Subject,Related,date,OpenTill,DiscountType,Favoritetags,Status, 
        Assigned,Toperson,Address,City,State,Country,ZipCode,Total,Quatation_items,Discount,Adjustment
      })
        .pipe(map(res => {
          return res;
        }));
    }
    //Update quotation api
    UpdateQuotation(
      apiURL: string,
      Email: string,
      Phone: string,
      Subject: string,
      Related:string,
      date:string, 
      OpenTill:string,
      DiscountType:string,
      Favoritetags:string,
      Status:string,
      Assigned: string,
      Toperson:string,
      Address: string,
      City: string,
      State: string,
      Country: string,
      ZipCode: string,
      Total: string,Discount:string,Adjustment:string) {
      return this.http.put<Quotation>(`${environment.apiUrl}/${apiURL}`, {
        Email,Phone,Subject,Related,date,OpenTill,DiscountType,Favoritetags,Status, 
        Assigned,Toperson,Address,City,State,Country,ZipCode,Total,Discount,Adjustment
      })
        .pipe(map(res => {
          return res;
        }));
    }

    // Add Quotation Item
    AddQuotationItem(Items:string,Description:string,Qty:string,Rate:string,Tax:string,quotationId:string){
      return this.http.post<QuotationItem>(`${environment.apiUrl}/quotationitem`, {
        Items,
        Description,
        Qty,
        Rate,
        Tax,
        quotationId
      })
      .pipe(map(res => {
        return res;
      }));
  }
    // update Quotation Item
    UpdateQuotationItem(apiURL: string,Items:string,Description:string,Qty:string,Rate:string,Tax:string,quotationId:string,amount:number){ 
      return this.http.put<QuotationItem>(`${environment.apiUrl}/${apiURL} `, {
        Items,
        Description,
        Qty,
        Rate,
        Tax,
        quotationId,
        amount
      })
      .pipe(map(res => {
        return res;
      }));
  }

  // Get quotation Items 
 getQuotationItem<T>(apiURL: string): Observable<T[]> {
  return this.mapAndCatchError(
    this.http.get<T[]>(`${this._apiEndPoint}/${apiURL}`)
  );
}  

    //Delete quotationItem  api
    DeleteQuotationItem<T>(apiURL: string): Observable<T[]> {
      return this.mapAndCatchError(
        this.http.delete<T[]>(`${this._apiEndPoint}/${apiURL}`, {
        })
      );
    }
    //Delete itemsOnly  api
      DeleteQuotationItemOnly<T>(apiURL: string): Observable<T[]> {
        return this.mapAndCatchError(
          this.http.delete<T[]>(`${this._apiEndPoint}/${apiURL}`, {
          })
        );
      }
    private mapAndCatchError<TData>(response: Observable<TData>): Observable<TData> {

      return response.pipe(
        map((r: TData) => {
  
          return r;
        }),
        catchError((error: HttpErrorResponse) => {
          return of(null);
        })
      );
    }

  
}
