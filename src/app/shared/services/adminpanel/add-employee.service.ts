import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Employee } from '../../models/adminpanel/employee';

@Injectable({
  providedIn: 'root'
})
export class AddEmployeeService {

  constructor(private http: HttpClient) { }


  //Add Employee api
  AddEmployee(email: string, name: string,
    phone: string, profile_image: string, facebook: string,
    linkedin: string, skype: string, language: string, password: string, role: string,
    quotationright:boolean,productright:boolean
  ) {
    return this.http.post<Employee>(`${environment.apiUrl}/employee`, {

      email, name,
      phone, profile_image,
      facebook, linkedin, skype,
      language, password, role,quotationright, productright
    })
      .pipe(map(res => {
        return res;
      }));
  }

  //Update Employee api
  UpdateEmployee(apiURL: string, id: string, email: string, name: string,
    phone: string, profile_image: string, facebook: string,
    linkedin: string, skype: string, language: string, password: string, role: string,
    quotationright:boolean,productright:boolean
  ) {
    return this.http.put<Employee>(`${environment.apiUrl}/${apiURL}`, {
      id, email, name,
      phone, profile_image,
      facebook, linkedin, skype,
      language, password, role ,quotationright , productright
    })
      .pipe(map(res => {
        return res;
      }));
  }
}
