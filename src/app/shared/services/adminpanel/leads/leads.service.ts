import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Leads } from 'src/app/shared/models/adminpanel/leads/leads';
import { RestmanagerService } from 'src/app/shared/utils/restmanagerService';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LeadsService {
  private restmanagerService: RestmanagerService;
  _apiEndPoint: string = '';

  constructor(private http: HttpClient,
    restmanagerService: RestmanagerService) {
    this.restmanagerService = restmanagerService;
    this._apiEndPoint = environment.Url;
  }

  //Get leads-list api
  getLeads() {
    return this.http.get<Leads[]>(`${environment.apiUrl}/leads`);
  }

  //Get Employee leads-list api
  getEmployeeLeads(apiURL) {
    return this.http.get<Leads[]>(`${environment.apiUrl}/${apiURL}`);
  }

  //Get lead data by id api
  getLeaddatabyid<T>(apiURL: string): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.get<T[]>(`${this._apiEndPoint}/${apiURL}`)
    );
  }
  private mapAndCatchError<TData>(response: Observable<TData>): Observable<TData> {

    return response.pipe(
      map((r: TData) => {

        return r;
      }),
      catchError((error: HttpErrorResponse) => {
        return of(null);
      })
    );
  }


  //Add Lead api
  AddLead(name: string, CompanyName: string, email: string, Phone: string, whatsappnumber: string,
    leadvalue: string, tags: string, assigneuserid: string, assigned: string, status: string, address: string, city: string,
    state: string, country: string, zip: string, language: string, description: string, date_contact: string,
    sources: string, position: string, website: string, service_category: string, aadharcard_no: string) {
    return this.http.post<Leads>(`${environment.apiUrl}/leads`, {

      name, CompanyName, email, Phone,
      whatsappnumber, leadvalue, tags, assigneuserid, assigned, status, address,
      city, state, country, zip, language, description, date_contact,
      sources, position, website, service_category, aadharcard_no
    })
      .pipe(map(res => {
        return res;
      }));
  }

  //Update Lead api
  UpdateLead(apiURL: string, id: string, leadId: string, name: string, CompanyName: string,
    email: string, Phone: string, whatsappnumber: string, leadvalue: string,
    tags: string, assigneuserid: string, assigned: string, status: string, address: string, city: string, state: string,
    country: string, zip: string, language: string, description: string, date_contact: string,
    sources: string, position: string, website: string, service_category: string, aadharcard_no: string) {
    return this.http.put<Leads>(`${environment.apiUrl}/${apiURL}`, {

      id, leadId, name, CompanyName, email, Phone,
      whatsappnumber, leadvalue, tags, assigneuserid, assigned, status, address,
      city, state, country, zip, language, description, date_contact,
      sources, position,
      website, service_category,
      aadharcard_no
    })
      .pipe(map(res => {
        return res;
      }));
  }

  //Delete Lead  api
  DeleteLead<T>(apiURL: string): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.delete<T[]>(`${this._apiEndPoint}/${apiURL}`, {
      })
    );
  }


  //change Lead status  api
  ChangeLeadStatus<T>(apiURL: string, status: any): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.put<T[]>(`${this._apiEndPoint}/${apiURL}`, {
        status
      })
    );
  }



  //Assign Employee status  api
  AssignedEmployee<T>(apiURL: string, assigned: any, assigneuserid: any): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.put<T[]>(`${this._apiEndPoint}/${apiURL}`, {
        assigned,
        assigneuserid
      })
    );
  }

  ImportLead(apiURL, formdata) {

    return this.http.post<Leads>(`${environment.apiUrl}/${apiURL}`,
      formdata

    )
      .pipe(map(res => {
        return res;
      }));
  }

}
