import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Reminders } from 'src/app/shared/models/adminpanel/leads/reminders';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RemindersService {
  _apiEndPoint: string = '';

  constructor(private http: HttpClient) {
    this._apiEndPoint = environment.Url;
  }

  //Add Lead reminder api
  AddLeadReminder(
    leadId,
    reminder_date,
    reminder_to, description, send_mail) {
    return this.http.post<Reminders>(`${environment.apiUrl}/leadreminder`, {
      leadId,
      reminder_date,
      reminder_to, description, send_mail
    })
      .pipe(map(res => {
        return res;
      }));
  }

  //Update Lead reminder api
  UpdateLeadReminder(
    apiURL,
    id,
    leadId,
    reminder_date,
    reminder_to, description, send_mail) {
    return this.http.put<Reminders>(`${environment.apiUrl}/${apiURL}`, {
      id,
      leadId,
      reminder_date,
      reminder_to, description, send_mail
    })
      .pipe(map(res => {
        return res;
      }));
  }

  //Get Reminder data by id api
  getReminderdatabyid<T>(apiURL: string): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.get<T[]>(`${this._apiEndPoint}/${apiURL}`)
    );
  }
  private mapAndCatchError<TData>(response: Observable<TData>): Observable<TData> {

    return response.pipe(
      map((r: TData) => {

        return r;
      }),
      catchError((error: HttpErrorResponse) => {
        return of(null);
      })
    );
  }

  //Delete Reminder  api
  DeleteReminder<T>(apiURL: string): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.delete<T[]>(`${this._apiEndPoint}/${apiURL}`, {
      })
    );
  }
}
