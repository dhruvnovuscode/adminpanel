import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Notes } from 'src/app/shared/models/adminpanel/leads/notes';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotesService {
  _apiEndPoint: string = '';
  constructor(private http: HttpClient) {
    this._apiEndPoint = environment.Url;
  }

  //Add Lead Notes api
  AddLeadNotes(leadId, note, date_contacted) {
    return this.http.post<any>(`${environment.apiUrl}/notes`, {
      leadId, note, date_contacted
    })
      .pipe(map(res => {
        return res;
      }));
  }

  //Update Lead note api
  UpdateLeadNotes(
    apiURL,
    id,
    note,
    date_contacted) {
    return this.http.put<Notes>(`${environment.apiUrl}/${apiURL}`, {
      id,
      note,
      date_contacted,

    })
      .pipe(map(res => {
        return res;
      }));
  }


  //Get Notes data by id api
  getNotesdatabyid<T>(apiURL: string): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.get<T[]>(`${this._apiEndPoint}/${apiURL}`)
    );
  }
  private mapAndCatchError<TData>(response: Observable<TData>): Observable<TData> {

    return response.pipe(
      map((r: TData) => {

        return r;
      }),
      catchError((error: HttpErrorResponse) => {
        return of(null);
      })
    );
  }

  //Delete Note  api
  DeleteNote<T>(apiURL: string): Observable<T[]> {

    return this.mapAndCatchError(
      this.http.delete<T[]>(`${this._apiEndPoint}/${apiURL}`, {
      })
    );
  }
}
